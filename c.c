/*

  program in c language
  for console
  it uses qd library ( libqd) for quad double precision 

  --------- description --------------
  https://commons.wikimedia.org/wiki/File:InfoldingSiegelDisk1over3.gif
  https://en.wikibooks.org/wiki/Fractals/Iterations_in_the_complex_plane/siegel#1.2F3

  program computes critial orbits 
  for series of paremeter : (n->t->c )
 
  for complex quadratic polynomial
  fc(z) = z^2 + c
  where parameter c is computed from integer n 
  near t=1/2 

  c = c(n) = c(t(n))
  -----------------------------
 
 


  ------- compile and run ---------------
  gcc c.c -lm -lqd -Wall -DUSE_QD_REAL -march=native
  ./a.out 





  ---------- git ------------------

  cd existing_folder
  git init
  git remote add origin git@gitlab.com:adammajewski/InfoldingSiegelDisk_in_c_1over2_quaddouble_distance.git
  git add c.c
  git commit -m " first commit"
  git push -u origin master


------------------ gnuplot -----------

gnuplot
set term png
set style line 1 lc rgb '#00ff0000' lt 1 lw 1 pt 7 ps 1.0 

set out "d0.png"
set title "distance between points of critical orbit for n=0   "
set xlabel "iteration/2"
set logscale y
set ylabel "distance(z_{n},z_{n+2})"
plot "d0.txt" title "distance" with linespoints ls 1  


set out "d1.png"
set title "distance between points of critical orbit for n=1   "
set xlabel "iteration/2"
set logscale y
set ylabel "distance(z_{n},z_{n+2})"
plot "d1.txt" title "distance" with linespoints  ls 1  


set out "d2.png"
set title "distance between points of critical orbit for n=2   "
set xlabel "iteration/2"
set logscale y
set ylabel "distance(z_{n},z_{n+2})"
plot "d2.txt" title "distance"  with linespoints  ls 1 


set out "d3.png"
set title "distance between points of critical orbit for n=3   "
set xlabel "iteration/2"
set logscale y
set ylabel "distance(z_{n},z_{n+2})"
plot "d3.txt" title "distance"  with linespoints  ls 1 


set out "d4.png"
set title "distance between points of critical orbit for n=4   "
set xlabel "iteration/2"
set xtics 5000
set logscale y
set ylabel "distance(z_{n},z_{n+2})"
plot "d4.txt" title "distance" with linespoints  ls 1 


set out "d5.png"
set title "distance between points of critical orbit for n=5   "
set xlabel "iteration/2"
set xtics 50000
set logscale y
set ylabel "distance(z_{n},z_{n+2})"
plot "d5.txt" title "distance" with linespoints  ls 1 

set out "d6.png"
set title "distance between points of critical orbit for n=6   "
set xlabel "iteration/2"
set xtics 500000
set logscale y
set ylabel "distance(z_{n},z_{n+2})"
plot "d6.txt" title "distance" with linespoints  ls 1 

set out "d7.png"
set title "distance between points of critical orbit for n=7   "
set xlabel "iteration/2"
set xtics 5000000
set logscale y
set ylabel "distance(z_{n},z_{n+2})"
plot "d7.txt" title "distance" with linespoints  ls 1 


set out "d8.png"
set title "distance between points of critical orbit for n=8   "
set xlabel "iteration/2"
set xtics 50000000
set logscale y
set ylabel "distance(z_{n},z_{n+2})"
plot "d8.txt" u 1:2 every 10000 title "distance" with linespoints  ls 1 

set out "d9.png"
set title "distance between points of critical orbit for n=9   "
set xlabel "iteration/2"
set xtics 500000000
set logscale y
set ylabel "distance(z_{n},z_{n+2})"
plot "d9.txt" u 1:2 every 10000 title "distance" with linespoints  ls 1 



---------------------
file d9.txt saved


n = 9 
t = 4.99999999750000000279508496874973712400532329685126669930742707e-01
cx = -7.49999999999999998149449178933702694145524879491347408142864355e-01
cy = 1.57079632503869293681972526129998193269783324874397448954896142e-09
iMaxForward = 3000000030


 ------------------------------------------ 


real	102m30.565s
user	101m8.340s
sys	0m58.656s





*/

#include <stdio.h>
#include <stdlib.h> // malloc
#include <string.h> // strcat
#include <math.h>   // pow -lm
#include <qd/c_qd.h> // qd library :  quad double number with precision = 64 decimal digits 














// rational number p/q 
// sequence of t(n) tends to p/q as n tends to infinity 
#define NUM 1
#define DEN 2





// n -> t -> c =cx+cy*i
int n;
double t[4];
double cx[4];
double cy[4];


// number of iterations 
unsigned long long int iMaxForward;



















// ------------- functions -------------------------------------

/* phi = (1+sqrt(5))/2 = golden ratio
 https://en.wikipedia.org/wiki/Golden_ratio

input = none
output = p 

*/
void GivePhi(double phi[4]){
 

  double a[4];
  
  


  c_qd_copy_d(5.0, phi); // phi = 5.0
  c_qd_copy_d(1.0, a); // a = 1.0

  c_qd_sqrt(phi, phi); // p = sqrt(phi) =sqrt(5)
  c_qd_add(phi, a, phi); // phi = a+phi = 1+sqrt(5)
  c_qd_selfmul_d(0.5, phi); // phi = phi*0.5 = phi/2 = phi
  
  
}



/*


  compute floating point number from continued fraction
  t(n) = [0;2,10^n,phi]

  = 1/ (2 + (1 /(p + (1/phi))))


input n
output t

*/



void GiveT(int n,  double t[4]){

  double p[4], a[4], b[4], one[4],  two[4];

  double phi[4];

  
  //
  c_qd_copy_d(1.0, one); // one = 1.0
  c_qd_copy_d(2.0, two); // two = 2.0
  c_qd_copy_d(10.0, a); // a = 10.0
  c_qd_npwr(a, n, p); // p = a^n
  GivePhi(phi); 
  c_qd_div(one,phi,b); // b= one/phi = 1/phi
  
  c_qd_add(p,b,t); // t= p+b = p+1/phi = (a^n) +1/phi
 
  c_qd_copy(one,b);
  c_qd_selfdiv(t,b); //b= 1/t
 
  c_qd_add(two, b, t); // t = 2+b   
 

  c_qd_copy(one,b);
  c_qd_selfdiv(t,one); //t= 1/b
 

  c_qd_copy(one,t); 
 

}



/* 
   compute complex number c = point on the boundary of period 1 component of Mandelbrot set 
  
   double a = t *2*M_PI; // from turns to radians
   double cx, cy; 
   // c = cx+cy*i 
   cx = (cos(a))/2-(cos(2a))/4; 
   cy = (sin(a))/2-(sin(2a))/4;

   input t
   output cx,cy  where c = cx + cy*I

*/
void GiveC( double t[4], double cx[4], double cy[4]){
  double  a[4];
  double a2[4];
  double  p[4];
  // 
  double   s[4];
  double  s2[4];



  c_qd_pi(p);
  
  c_qd_selfmul_d(2.0,p); // p = 2 *pi
  c_qd_mul(t, p,  a);

  c_qd_mul_qd_d(a, 2.0, a2); // a2 = 2*t*p
  //
  c_qd_cos(a,s);   // s  = cos(a)
  c_qd_cos(a2,s2); // s2 = cos(a2)
  //
  c_qd_selfdiv_d(2.0,s);   // s  = s/2
  c_qd_selfdiv_d(4.0,s2);  // s2 = s/4 
  //
  c_qd_sub(s,s2,cx); // cx = s - s2 

  //

  c_qd_sin(a,s);   // s  = sin(a)
  c_qd_sin(a2,s2); // s2 = sin(a2)
  //
  c_qd_selfdiv_d(2.0,s);   // s  = s/2
  c_qd_selfdiv_d(4.0,s2);  // s2 = s/4 
  //
  c_qd_sub(s,s2,cy); // cy = s - s2 




}




/*
The distance between (x1, y1) and (x2, y2) is given by:
https://en.wikipedia.org/wiki/Distance

*/

void c_qd_distance(const double *x1, const double *y1, const double *x2, const double *y2, double *d){

  double dx[4];
  double dy[4];
  double dx2[4];
  double dy2[4];
  double tmp[4];
  

  c_qd_sub(x1, x2, dx); // dx = x1 - x2
  c_qd_sub(y1, y2, dy); // dy = y1 - y2
  //
  c_qd_sqr(dx, dx2); // dx2 = dx * dx
  c_qd_sqr(dy, dy2); // dy2 = dy * dy;
  //
  c_qd_add(dx2, dy2, tmp); // tmp = dx2 + dy2
  c_qd_sqrt(tmp, d);  // d = sqrt(tmp);

}





/*

  forward iteration without explicit use of complex number 
  f(z) = z^2 + c = complex quadratic polynomial
  -----------------
  tmp = 2 * zx * zy + cy;
  zx= zx2 - zy2 + cx;
  zy = tmp;

  input: 
    z0 = z0x + z0y*i
    iMaxF


*/

int ForwardOrbit(int _n, const double z0x[4], const double z0y[4], unsigned long long int iMaxF, int jMax){


  // iteration = i*j
  unsigned long long int i; // outer loop
  int j; //inner loop 

  double tmp[4];
  
  double x[4];
  double y[4];
  double x2[4];
  double y2[4];

  double xOld[4];
  double yOld[4];

  double d[4]; // distance 
  


  // text file for distances 
  char d_name [100]; /* name of file */
  snprintf(d_name, sizeof d_name, "d%d", _n); /*  */
  char *d_filename =strncat(d_name,".txt", 4); 
  
  FILE *d_file;
  d_file = fopen(d_filename, "w");  
  if (d_file  == NULL) {
    fprintf(stderr, "Nie moge otworzyc %s\n", d_filename);
    return 1;}
   /* nag��wek */
    fprintf(d_file,"%s \t %s \t  %s \n","#","x","y");



   // z = z0
   c_qd_copy(z0x, x);
   c_qd_copy(z0y, y); 
   // zOld = z0 
   c_qd_copy(z0x, xOld);
   c_qd_copy(z0y, yOld); 


 
  for (i=0; i<iMaxF; ++i) {



  // check only n-th iteration  where n = DEN = denominator of internal angle 
  for (j=0; j<jMax ; ++j ){ 
    
    // manual debug
    //printf("i = %llu \n", i);
    //printf("xOld = "); c_qd_write(xOld); 
    //printf("yOld = "); c_qd_write(yOld); 

    // tmp = 2 * xOld * yOld + cy;
    c_qd_mul_qd_d(x, 2.0, tmp); // tmp = 2*x
    c_qd_selfmul(y, tmp);   // temp  = temp*y = 2*x*y
    c_qd_selfadd(cy, tmp);   // temp  = temp+cy = 2*x*y +cy

    // x= x2 - y2 + cx;
    c_qd_sqr(x, x2); // x2 = x*x
    c_qd_sqr(y, y2); // y2 = y*y
    c_qd_sub(x2,y2, x); // x = x2-y2
    c_qd_selfadd(cx, x);   // x  <- x + cx = x2-y2 +cx
    
    // y = tmp;
     c_qd_copy(tmp, y); 
    
    // no escape ( bailout ) test !!
    
    
    }// for (j ...

    // distance between new and old z 
    c_qd_distance(x, y, xOld, yOld, d); // compute
    fprintf(d_file, "%llu \t  %.16f \n", i, d[0]);  // save to the file only first 16 digits = double 
      
    // zOld = z
    c_qd_copy(x, xOld); 
    c_qd_copy(y, yOld); 

  }// for (i .... 


  
  // text file for distances 
  fclose(d_file);
  fprintf(stderr,"file %s saved\n", d_filename);

  return 0;
}



int CriticalOrbit(int _n, unsigned long long int iMax_F){

  
  double  z0x[4];
  double  z0y[4];
  
  // critical point z = zx+zy*i = 0
  c_qd_copy_d(0.0, z0x); // zx= = 0.0
  c_qd_copy_d(0.0, z0y); // zy = 0.0

  ForwardOrbit(_n,  z0x,z0y, iMax_F, DEN); // forward iteration of critical point 
  
 




  return 0;
}


unsigned long long int Give_iMaxForward( int n){
   
  unsigned long long int i ;
  i = 3.0*pow(10,n) + 30;

 /*
  switch( n )
    { case  0 : i =       60; break;
      case  1 : i =       60; break;  
      case  2 : i =      300; break;
      case  3 : i =     3000; break;
      case  4 : i =    20000; break;
      case  5 : i =   200000; break;
      case  6 : i =  1600000; break;
      case  7 : i =  2000000; break;
      case  8 : i =    20000; break;
      case  9 : i =    20000; break;
      case 10 : i =    20000; break;
      case 11 : i =    20000; break;
    default: i = 3000; break;
    }
  */
  return i;
 
}



// prints out important informations
void info(int n_){

   // text file for info 
  char name [100]; /* name of file */
  snprintf(name, sizeof d_name, "d%d_info", _n); /*  */
  char *filename =strncat(d_name,".txt", 4); 
  
  FILE *file;
  d_file = fopen(filename, "w");  
  if (file  == NULL) {
    fprintf(stderr, "Nie moge otworzyc %s\n", filename);
    return 1;}


  
  fprintf(file, "n = %d \n", n_ );
  fprintf(file, "t = "); c_qd_write(t); 
  fprintf(file, "cx = "); c_qd_write(cx); 
  fprintf(file, "cy = "); c_qd_write(cy);  
  fprintf(file, "iMaxForward = %llu\n", iMaxForward );
  
  


 // text file for info
  fclose(file);
  fprintf(stderr,"file %s saved\n", filename);

}








void Compute_Save(int _n){

  // n -> t
  GiveT(_n,t); // from continued fraction to floating point (decimal expansion)
  
  // t -> c = from rotation number ( internal angle) to complex number c 
  GiveC(t, cx,cy);

  // c -> critical orbit 
  iMaxForward   = Give_iMaxForward(_n);
  
  CriticalOrbit( _n, iMaxForward);
  

  info(_n);


}








// init = all procedures before start of main computations 
int setup(void){


 
  fpu_fix_start(NULL); // libqd : turns on the round-to-double bit in the FPU control word on Intel x86 Processors. 
  return 0;
}





// all procedures before end of the program
void terminate(void){

  //
  fpu_fix_end(NULL); // libqd : 
  
  

}




// ------------------------------------------------------------------------------------------------------------------------

int main(void) {

 


  setup(); 
 

 // for t tending to p/q
  for (n=9; n<10; n++)  
    Compute_Save(n); 
 
 
 
 
  terminate();

  return 0; 
}
